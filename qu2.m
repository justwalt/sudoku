function p = qu2(p,q)
for k = 1:9
    for j = 1:3
        for i = 1:3
            r = find(p(3*i-2:3*i,3*j-2:3*j,k));
            
            if length(r) == 1
                % indeces
                ini = mod(r,3);
                if ini == 0
                    ini = 3;
                end
                inj = ceil(r/3);
                
                ovi = 3*i - 2 + (ini - 1);
                ovj = 3*j - 2 + (inj - 1);
                
                % updating value
                p(ovi,ovj,1:k-1) = zeros(1,1,k-1);
                p(ovi,ovj,k+1:9) = zeros(1,1,9-k);
            end
        end
    end
end
end
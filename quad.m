function k = qu(i,j)
row = mod(i,3);
col = ceil(j/3);

if row == 0
    row = 3;
end

k = row + 3*col - 3;

end
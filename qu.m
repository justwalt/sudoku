function k = qu(i,j)
if i >= 1 && i <= 3
    row = 1;
elseif i >= 4 && i <= 6
    row = 2;
elseif i >= 7 && i <= 9
    row = 3;
end

if j >= 1 && j <= 3
    col = 1;
elseif j >= 4 && j <= 6
    col = 2;
elseif j >= 7 && j <= 9
    col = 3;
end

k = row + 3*col - 3;

end
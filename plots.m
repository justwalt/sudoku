%% test puzzles
q = [0 9 0 1 0 0 0 0 0;
     0 0 0 0 0 5 0 0 0;
     0 0 0 0 0 0 9 0 0;
     1 2 0 0 5 0 0 0 0;
     9 0 3 0 0 0 7 0 6;
     0 0 0 0 3 0 0 5 2;
     0 0 1 0 0 0 0 0 0;
     0 0 0 7 0 0 0 0 0;
     0 0 0 0 0 4 0 2 0];        % requires lots of sequential guessing, takes a while
 
u = [0 0 0 3 0 0 9 0 0;
     0 3 0 0 5 9 0 2 7;
     0 4 0 2 1 8 0 0 6;
     0 6 0 0 7 1 0 5 0;
     0 1 4 0 0 0 3 9 0;
     0 5 0 9 4 0 0 7 0;
     1 0 0 6 8 2 0 4 0;
     4 2 0 7 3 0 0 8 0;
     0 0 8 0 0 4 0 0 0];        % 
 
x = [5 7 8 3 6 1 4 9 2;
     2 3 6 7 4 9 1 8 5;
     9 1 4 5 8 2 6 7 3;
     0 4 3 0 2 0 7 5 0;
     0 8 5 0 7 3 2 4 0;
     7 2 9 0 5 4 3 6 0;
     3 5 7 0 1 0 9 2 4;
     4 6 1 2 9 5 8 3 7;
     8 9 2 4 3 7 5 1 6];        % will be pure guessing but finish quickly
 
w = [5 0 0 0 0 4 0 0 0;
     9 0 0 0 0 0 0 6 4;
     0 0 0 7 8 0 3 0 0;
     0 0 6 0 9 0 1 0 0;
     0 5 0 3 0 1 0 8 0;
     0 0 3 0 6 0 5 0 0;
     0 0 4 0 2 8 0 0 0;
     3 1 0 0 0 0 0 0 7;
     0 0 0 1 0 0 0 0 8];        % another one with a lot of guessing
 
j = [7 0 0 0 0 2 0 6 0;
     0 0 2 9 6 0 0 0 0;
     0 0 0 0 0 0 0 4 8;
     0 4 3 0 0 1 0 0 0;
     0 0 0 2 0 3 0 0 0;
     0 0 0 6 0 0 8 2 0;
     6 5 0 0 0 0 0 0 0;
     0 0 0 0 5 6 7 0 0;
     0 1 0 8 0 0 0 0 2];        % lots of guessing
 
b = [0 0 0 0 0 0 0 0 0;
     0 0 0 0 0 3 0 8 5;
     0 0 1 0 2 0 0 0 0;
     0 0 0 5 0 7 0 0 0;
     0 0 4 0 0 0 1 0 0;
     0 9 0 0 0 0 0 0 0;
     5 0 0 0 0 0 0 7 3;
     0 0 2 0 1 0 0 0 0;
     0 0 0 0 4 0 0 0 9];        % backtracking killer

z = zeros(9,9);                 % will take forever but be successful (will always result
                                % in the same answer matrix

%% solver line
% change out the variable for different puzzles
tic
a = sudoku(b)
toc

%% plotting
% map1 is a collection of color codes
map1 = [0.9 0 0;          % red
       1 0.5 0;          % orange
       0.9 0.9 0;        % yellow
       0 0.8 0;          % green
       0 0.2 0.9;        % blue
       0.7 0.1 1;        % purple
       0.9 0.9 0.9;      % white
       0.5 0.5 0.5;      % gray
       0.1 0.1 0.1;      % black
       1 1 1;];          % white

% this plots the image of the puzzle
set(0,'DefaultFigureWindowStyle','docked')
close all
figure
image([a 10*ones(9,2) [1 2 3 4 5 6 7 8 9]'])
colormap(map1)
%axis([0.5 11.5 0.5 9.5])
%legend('1','2','3','4','5','6','7','8','9')

% this adds the black lines to the puzzle
hold on
plot([3.5 3.5],[0.5 9.5],'k-','LineWidth',5)
plot([6.5 6.5],[0.5 9.5],'k-','LineWidth',5)
plot([0.5 9.5],[3.5 3.5],'k-','LineWidth',5)
plot([0.5 9.5],[6.5 6.5],'k-','LineWidth',5)

% this lets you know if the puzzle was solved
solved = issolved(a);
if solved == 1
    disp('The puzzle was successfully solved.')
elseif solved == 0
    disp('The puzzle was not successfully solved.')
end
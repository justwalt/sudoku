%% Sudoku Checker
function  TF = check(z)

p = [1 2 3 4 5 6 7 8 9];
m = zeros(1,9);
TF = 1;
%% Square Uniqueness
%if TF == 1
for j = 1:3
    for i = 1:3
        % get the range of values from u that correspond to the same local
        % square (1:3,1:3; 4:6,1:3; and so on)
        o = z(3*i-2:3*i,3*j-2:3*j);
        u = unique(o)';
        if length(u) == length(p)
            % Option to print which box invalid
            % fprintf('Box (%d,%d) Has No Duplicates\n',i,j)
            
        else if length(u) ~= length(p)
                % fprintf('Box (%d,%d) Invalid\n',i,j)
                TF = 0;
                break;
            end
        end
    end
end
%end
%% Rows Uniqueness

for i = 1:9
    for j = 1:9
        m(1,j) = z(i,j);
    end
    u = unique(m);
    if (length(u) == length(p))
        %fprintf('Row %d Has No Duplicates\n',i)
        
    elseif length(u)~= length(p)
            %fprintf('Row %d Invalid\n', i)
            TF = 0;
            break;
    end
end


%% Column Uniqueness
%similar to row with the loops "swapped" to get the columns

for i = 1:9
    for j = 1:9
        m(1,j) = z(j,i);
    end
    u = unique(m);
    if length(u) == length(p)
        % fprintf('Column %d Has No Duplicates\n',i)
    elseif length(u)~= length(p)
            %fprintf('Column %d Invalid\n',i)
            TF = 0;
            break;
    end
    
end



%% Box Sum

for j = 1:3
    for i = 1:3
        % get the range of values from u that correspond to the same local
        % square (1:3,1:3; 4:6,1:3; and so on)
        o = z(3*i-2:3*i,3*j-2:3*j);
        u = unique(o)';
        if (sum(u(1,:)) == 45)
            % fprintf('Box (%d,%d) complete\n',i,j)
            %TF = 1;
        elseif (sum(u(1,:)) ~= 45)
                % fprintf('Box (%d,%d) Not Valid\n',i,j)
                TF = 0;
                break;
        end
    end
end
%% Row Sum
for i = 1:9
    for j = 1:9
        m(1,j) = z(i,j);
    end
    
    if (sum(m(1,:)) == 45)
        %disp('Row = 45')
        
    elseif sum(m(1,:)) ~= 45
            %disp('Row Invalid')
            TF = 0;
            break;
    end
end

%% Column Sum

for i = 1:9
    for j = 1:9
        m(1,j) = z(j,i);
    end
    
    if sum(m(1,:)) == 45
        % disp('Column = 45')
    elseif sum(m(1,:)) ~= 45
            % disp('Column Invalid')
            TF = 0;
            break;
    end
end

%% Testers

% test puzzlez
% u = [0 0 0 3 0 0 9 0 0;
%     0 3 0 0 5 9 0 2 7;
%     0 4 0 2 1 8 0 0 6;
%     0 6 0 0 7 1 0 5 0;
%     0 1 4 0 0 0 3 9 0;
%     0 5 0 9 4 0 0 7 0;
%     1 0 0 6 8 2 0 4 0;
%     4 2 0 7 3 0 0 8 0;
%     0 0 8 0 0 4 0 0 0]

%        z = [ 9 2 4 9 5 3 6 7 1
%              6 3 5 8 1 7 9 2 4
%              7 1 9 6 2 4 8 5 3
%              5 8 7 2 9 1 3 4 6
%              1 4 2 7 3 6 5 8 9
%              3 9 6 4 8 5 2 1 7
%              2 6 1 5 4 9 7 3 8
%              4 7 8 3 6 2 1 9 5
%              9 5 3 1 7 8 4 2 6 ];

% z = [2 9 5 7 4 3 8 6 1;
%     4 3 1 8 6 5 9 2 7;
%     8 7 6 1 9 2 5 4 3;
%     3 8 7 4 5 9 2 1 6;
%     6 1 2 3 8 7 4 9 5;
%     5 4 9 2 1 6 7 3 8;
%     7 6 3 5 3 4 1 8 9; %this row incorrect
%     9 2 8 6 7 1 3 5 4;
%     1 5 4 9 3 8 6 7 2];
%             % This column incorrect

% z = [5 3 4 6 7 8 9 1 2;  Correct Puzzle
%     6 7 2 1 9 5 3 4 8;
%     1 9 8 3 4 2 5 6 7;
%     8 5 9 7 6 1 4 2 3;
%     4 2 6 8 5 3 7 9 1;
%     7 1 3 9 2 4 8 5 6;
%     9 6 1 5 3 7 2 8 4;
%     2 8 7 4 1 9 6 3 5;
%     3 4 5 2 8 6 1 7 9];

// This is a sudoku solver who uses a brute force method to solve puzzles. Inputs are
// given as strings of values, where blank spaces are represented by 0s, starting in the
// top left corner and continuing over the rows.

use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::process;

fn main() {
    // Get the list of input files from the command line.
    let file_list = get_file_list();

    // Generate a queue of files to be solved from the file list.
    let puzzle_queue = Queue::new(file_list);

    for mut puzzle in puzzle_queue {
        puzzle.solve();
        display_puzzle(&puzzle, "pretty");
    }
}

#[derive(Clone)]
struct Cell {
    value: Option<usize>,
    options: Vec<usize>,
}

impl Cell {
    fn new(value: Option<usize>) -> Cell {
        let mut options: Vec<usize>;
        if value.is_some() {
            let number = value.unwrap();
            options = vec![0; 9];
            options[number - 1] = number;
        } else {
            options = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];
        }

        Cell { value, options }
    }

    fn remove_options(&mut self, values: Vec<usize>) {
        // This method updates the cell's options vector by removing the given value.
        // dbg!(&values);
        // Only perform this step if the value is blank.
        if self.value.is_none() {
            for value in values {
                let index = match value {
                    1..=9 => value - 1,
                    _ => unreachable!(),
                };

                self.options[index] = 0;
            }
        }
    }

    fn check_if_one_option(&mut self) -> bool {
        // If there's only one value in the options vector, then update the value.
        let mut counter = 0;
        let mut newval = 0;
        for opt in &self.options {
            if *opt != (0 as usize) {
                counter += 1;
                newval = *opt;
            }
        }

        if newval == 0 {
            println!("{:?}", &self.options);
        }

        if counter > 1 {
            // Don't update
        } else if counter == 0 {
            panic!("Illegal value somewhere in puzzle.");
        } else {
            self.value = Some(newval);
        }

        self.value.is_some()
    }

    fn guess(&mut self) -> bool {
        // This method is meant to be called when the solve loop has been exhausted
        // and a guess needs to be made to solve the puzzle.
        let mut choice: usize = 0;
        let mut index: usize = 9;
        let mut made_choice = false;
        for i in 0..9 {
            if i != 0 {
                choice = i + 1;
                index = i;
                made_choice = true;
                break;
            }
        }

        if made_choice {
            let mut new_options = vec![0; 9];
            new_options[index] = choice;
            self.options = new_options;
            true
        } else {
            false
        }
    }
}

#[derive(Clone)]
struct Puzzle {
    name: String,
    board: Vec<Vec<Cell>>,
    solved: bool,
}

impl Puzzle {
    fn new(board: Vec<Vec<Cell>>, name: String) -> Puzzle {
        Puzzle {
            name,
            board,
            solved: false,
        }
    }

    fn return_row(&self, row: usize) -> Vec<usize> {
        // returns the specified row, taking values between 0-8 as the index
        let mut result_row: Vec<usize> = vec![];
        for i in 0..9 {
            if self.board[row][i].value.is_some() {
                let result_val = self.board[row][i].value.unwrap();
                result_row.push(result_val);
            }
        }

        result_row
    }

    fn return_col(&self, col: usize) -> Vec<usize> {
        // return the specified column, taking values between 0-8 as the index
        let mut result_col: Vec<usize> = Vec::new();
        for i in 0..9 {
            if self.board[i][col].value.is_some() {
                let result_val = self.board[i][col].value.unwrap();
                result_col.push(result_val);
            }
        }

        result_col
    }

    fn return_square(&self, square: usize) -> Vec<usize> {
        // The squares are accessed as if they were a simple vector. Their indices
        // correspond to the overall board as such:
        //
        //   ┏━━━┳━━━┳━━━┓
        //   ┃ 0 ┃ 1 ┃[2]┃----- ━┳━┯━┯━┓  Zooming in on the corner square here, it is
        //   ┣━━━╋━━━╋━━━┫\      ┃0│1│2┃  easy to see how the indices in the vector of
        //   ┃ 3 ┃ 4 ┃ 5 ┃ \    ─╂─┼─┼─┨  Cells that are returned are organized. For
        //   ┣━━━╋━━━╋━━━┫  \    ┃3│4│5┃  simplicity, it's set up the same way as the
        //   ┃ 6 ┃ 7 ┃ 8 ┃   \  ─╂─┼─┼─┨  indices of the squares.
        //   ┗━━━┻━━━┻━━━┛    \  ┃6│7│8┃
        //                      ━╋━┿━┿━┫

        let row_indices: Vec<usize>;
        let col_indices: Vec<usize>;

        match square {
            0..=2 => row_indices = [0, 1, 2].to_vec(),
            3..=5 => row_indices = [3, 4, 5].to_vec(),
            6..=8 => row_indices = [6, 7, 8].to_vec(),
            _ => panic!("Bad index for square check."),
        }

        match square {
            0 | 3 | 6 => col_indices = [0, 1, 2].to_vec(),
            1 | 4 | 7 => col_indices = [3, 4, 5].to_vec(),
            2 | 5 | 8 => col_indices = [6, 7, 8].to_vec(),
            _ => panic!("Bad index for square check."),
        }

        let mut result_sq: Vec<usize> = Vec::new();
        for i in row_indices {
            for j in col_indices.clone() {
                if self.board[i][j].value.is_some() {
                    let result_val = self.board[i][j].value.unwrap();
                    result_sq.push(result_val);
                }
            }
        }

        result_sq
    }

    fn solve(&mut self) {
        // This iterates over all of the puzzle's cells until they're all solved.
        let max_iters = 4096;
        let mut iters = 0;
        loop {
            let mut cells_not_solved = 0;
            for row in 0..9 {
                for col in 0..9 {
                    let vals_row = &self.return_row(row);
                    let vals_col = &self.return_col(col);
                    let squ = which_square(row, col);
                    let vals_squ = &self.return_square(squ);

                    let cell = &mut self.board[row][col];

                    cell.remove_options(vals_row.to_vec());
                    cell.remove_options(vals_col.to_vec());
                    cell.remove_options(vals_squ.to_vec());
                    if !cell.check_if_one_option() {
                        cells_not_solved += 1;
                    }
                }
            }

            if cells_not_solved == 0 {
                self.solved = true;
                println!("Solved puzzle {}", self.name);
                break;
            }

            iters += 1;
            if iters > max_iters {
                // DEBUG:
                for row in 0..9 {
                    for col in 0..9 {
                        let cell = &self.board[row][col];
                        println!("{:?}: {:?}", cell.value, cell.options);
                    }
                }
                // :DEBUG

                println!("Couldn't solve puzzle {}", self.name);
                break;
                // Here, instead of breaking, a clone of the puzzle should be made so
                // that it can take guesses of its own. Then, the idea is that if a
                // complete and legal solution is found, then it is returned up the
                // stack to here. And if a solution can't be found, then a different
                // guess is made and then things continue.
            }
        }
    }
}

struct Queue {
    list: Vec<Puzzle>,
}

impl Queue {
    fn new(file_list: Vec<String>) -> Queue {
        // This function takes the file names from the get_file_list function and converts
        // each of them to Puzzle objects, which will be returned as a Queue.

        let mut future_queue: Vec<Puzzle> = Vec::new();

        for file in file_list {
            // Iterating over each file ...
            let puzzle_file = match File::open(file.clone()) {
                Ok(file) => file,
                Err(err) => {
                    println!("{}", err);
                    panic!("An invalid file was passed.");
                }
            };

            let puzzle_reader = BufReader::new(&puzzle_file);
            let mut future_puzzle: Vec<Vec<Cell>> = Vec::new();

            for line in puzzle_reader.lines() {
                // ... we iterate over each line ...
                let row_string = match line {
                    Ok(row_vals) => row_vals,
                    Err(err) => {
                        println!("{}", err);
                        // panic!("This file contains an invalid line!");
                        panic!("{} contains an invalid line.", file.clone());
                    }
                };

                // ... and convert the chars to vectors of Cell objects ...
                let mut future_row: Vec<Cell> = Vec::new();

                let char_vec: Vec<char> = row_string.chars().collect();

                for chara in char_vec {
                    let digit = match chara {
                        '1'..='9' => Some(chara.to_digit(10).unwrap() as usize),
                        '0' | '_' => None,
                        _ => {
                            println!(
                                "Illegal character found: {}. Interpreting as blank cell..",
                                chara
                            );
                            None
                        }
                    };

                    future_row.push(Cell::new(digit));
                }

                // ... and then into a vector of vectors ...
                future_puzzle.push(future_row);
            }

            // ... which can be turned into Puzzle objects ...
            let new_puzzle = Puzzle::new(future_puzzle, file);

            // ... and finally into a vector of puzzles, which is a Queue ...
            future_queue.push(new_puzzle);
        }

        // ... and we're finished.
        Queue { list: future_queue }
    }

    fn pop_puzzle(&mut self) -> Option<Puzzle> {
        // Returns the first puzzle in the queue and then pops it from the queue.
        if !self.list.is_empty() {
            Some(self.list.pop().unwrap())
        } else {
            None
        }
    }
}

impl Iterator for Queue {
    type Item = Puzzle;

    fn next(&mut self) -> Option<Puzzle> {
        let next_puzzle = self.pop_puzzle();
        match next_puzzle {
            Some(next) => Some(next),
            None => None,
        }
    }
}

fn get_file_list() -> Vec<String> {
    // This function handles the input arguments which are intended to be filenames,
    // which contain 9 lines of 9 numbers each. You may have noticed this happens to be
    // the shape of a sudoku board, which is convenient because this is a sudoku solver.

    // Example usage:
    //     ./sudoku test_puzzle_1.txt test_puzzle_2.txt
    //
    // Result from this function:
    //     ["test_puzzle_1.txt", "test_puzzle_2.txt"]

    let args: Vec<String> = env::args().collect();
    let mut file_list: Vec<String> = Vec::new();

    match args.len() {
        1 => {
            println!("We need at least one input filename here! They should contain 9x9");
            println!("Sudoku puzzles with the blank spaces represented as 0s or spaces.");

            process::exit(1);
        }
        _ => {
            // TODO: print out the filenames in the right order (not reversed, as the
            // loop sees them?
            for i in (1..args.len()).rev() {
                let file_name: String = args[i].clone();

                println!("Read file: {}", file_name);
                file_list.push(file_name);
            }
        }
    }

    file_list
}

fn which_square(row: usize, col: usize) -> usize {
    // Calculates square number from row and col indices.
    let row_opts: Vec<usize> = match row {
        0..=2 => vec![0, 1, 2],
        3..=5 => vec![3, 4, 5],
        6..=8 => vec![6, 7, 8],
        _ => unreachable!(),
    };

    let col_opts: Vec<usize> = match col {
        0..=2 => vec![0, 3, 6],
        3..=5 => vec![1, 4, 7],
        6..=8 => vec![2, 5, 8],
        _ => unreachable!(),
    };

    for r in row_opts {
        for c in col_opts.clone() {
            if r == c {
                return c;
            }
        }
    }

    panic!("No square value was found?!");
}

fn display_puzzle(puzzle: &Puzzle, style: &str) {
    // This function pretty-prints a puzzle to the command line.

    let opt2str = |i: Option<usize>| -> String {
        match i {
            Some(val) => val.to_string(),
            None => " ".to_string(),
        }
    };

    if style == "pretty" {
        println!("    {}", puzzle.name);
        println!(
            "  ┏━━━┯━━━┯━━━┳━━━┯━━━┯━━━┳━━━┯━━━┯━━━┓"
        );

        for i in 0..9 {
            println!(
                "  ┃ {} │ {} │ {} ┃ {} │ {} │ {} ┃ {} │ {} │ {} ┃",
                opt2str(puzzle.board[i][0].value),
                opt2str(puzzle.board[i][1].value),
                opt2str(puzzle.board[i][2].value),
                opt2str(puzzle.board[i][3].value),
                opt2str(puzzle.board[i][4].value),
                opt2str(puzzle.board[i][5].value),
                opt2str(puzzle.board[i][6].value),
                opt2str(puzzle.board[i][7].value),
                opt2str(puzzle.board[i][8].value)
            );

            if (i == 2) || (i == 5) {
                println!(
                    "  ┣━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┫"
                );
            } else if i == 8 {
                println!(
                    "  ┗━━━┷━━━┷━━━┻━━━┷━━━┷━━━┻━━━┷━━━┷━━━┛"
                );
                println!();
            } else {
                println!(
                    "  ┠───┼───┼───╂───┼───┼───╂───┼───┼───┨"
                );
            }
        }
    } else if style == "simple" {
        println!("{}", puzzle.name);

        for i in 0..9 {
            for j in 0..8 {
                let value = opt2str(puzzle.board[i][j].value);
                print!("{}", value);
            }
            let value = opt2str(puzzle.board[i][8].value);
            println!("{}", value);
        }
    }
}

function s = issolved(u)
%% rows
rr = 0;
cc = 0;
ss = 0;
s = 0;



for i = 1:9
    unr = find(unique(u(i,:)));
    row1 = length(unr);              % length of unique  (9)
    row2 = sum(u(i,:));              % sum of values     (45)
    row3 = histc(u(i,:),unr);        % broken test       (-)
    
    if row1 == 9 && row2 == 45
        rr = 1;
    elseif length(unique(row3)) > 1
        s = 0;
        return
    end
end

%% columns
for i = 1:9
    unc = find(unique(u(:,i)));
    col1 = length(unc);              % length of unique  (9)
    col2 = sum(u(:,i));              % sum of values     (45)
    col3 = histc(u(:,i),unc);        % broken test       (-)
    
    if col1 == 9 && col2 == 45
        cc = 1;
    elseif length(unique(col3)) > 1
        s = 0;
        return
    end
end

for j = 1:3
    for i = 1:3
        % get the range of values from u that correspond to the same local
        % square (1:3,1:3; 4:6,1:3; and so on)
        squ = u(3*i-2:3*i,3*j-2:3*j);
        squ = [squ(1:3) squ(4:6) squ(7:9)];
        uns = find(unique(squ));
        sq1 = numel(uns);
        sq2 = sum(squ);
        sq3 = histc(squ,uns);
        
        if sq1 == 9 && sq2 == 45
            ss = 1;
        elseif length(unique(sq3)) > 1
            s = 0;
            return
        end
    end
end

if rr == 1 && cc == 1 && ss == 1
    s = 1;
end
end
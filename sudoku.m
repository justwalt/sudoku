function u = sudoku(u,outer)
% Sudoku solver
% sudoku(u)
% u = 9x9 incoming matrix, empty spaces represented as 0s.
if nargin < 2
    outer = 1;
end

%% initialize framework
% all possible values 9x9x9 matrix
p = zeros(9,9,9);
for j = 1:9
    for i = 1:9
        p(i,j,:) = [1 2 3 4 5 6 7 8 9];
    end
end

% all possible values inside each 3x3 local square
q = [1 2 3 4 5 6 7 8 9]';
q = [q q q q q q q q q];

%% update framework to current input
% create q for local square possible values
for j = 1:3
    for i = 1:3
        % get the range of values from u that correspond to the same local
        % square (1:3,1:3; 4:6,1:3; and so on)
        o = u(3*i-2:3*i,3*j-2:3*j);
        rem = o(find(o));           % remaining values
        for k = 1:length(rem)
            q(rem(k),i+3*j-3) = 0;
        end
    end
end

% remove the values just found in q from p
for n = 1:81
    % convert numbers 1:81 to indices from 1:9,1:9
    row = mod(n,9);
    if row == 0
        row = 9;
    end
    col = ceil(n/9);
    
    % assign correct row value of q to k for indexing
    k = qu(row,col);
    
    % remove the possibilities from p
    if length(find(p(row,col,:))) == 9
        p(row,col,:) = q(:,k);
    end
    
    % if a cell came with a value, remove all except that value from p
    if u(row,col) ~= 0
        p(row,col,:) = zeros(9,1);              % set that column to 0
        p(row,col,u(row,col)) = u(row,col);     % put the original value back
    end
end

%% logic loops
iter = 0;
while iter < 150            % 150 iterations should be enough.. check(u) ~= 0 && 
    %% removes possibilities that are forbidden in row and column
    for col = 1:9
        for row = 1:9
            e = u(row,col);
            if e ~= 0
                p(row,:,e) = zeros(9,1);        % remove row possibilities
                p(:,col,e) = zeros(9,1);        % remove col possibilities
                p(row,col,e) = e;               % put the original value back
            end
        end
    end
    
    %% local squares
    a(:,:,1) = [1 1 1; 4 4 4; 7 7 7];           % pre-indexing
    a(:,:,2) = [1 4 7; 1 4 7; 1 4 7];           % pre-indexing
    
    % for local squares
    for s = 1:9
        % s is the index of the 3x3 local square, 1-9
        for k = 1:9
            % k will be the value in the cell
            w = p(a(s):a(s)+2,a(s+9):a(s+9)+2,k);
            if length(find(w)) == 1
                % then remove those possible from the local square
                q(k,s) = 0;
                % this does nothing on the first set, as nothing has
                % changed since the initial creation of q.
            end
        end
    end
    % commit the changes by looking at solo values in 3x3 in p; u(5,1)
    % should be 7 based on row logic but has 2,7,8 listed in p while 7 is
    % the correct choice
    for k = 1:9
        for j = 1:3
            for i = 1:3
                r = find(p(3*i-2:3*i,3*j-2:3*j,k));
                
                % if a square has only one possible location for a number..
                if length(r) == 1
                    % local square indeces
                    ini = mod(r,3);
                    if ini == 0
                        ini = 3;
                    end
                    inj = ceil(r/3);
                    
                    % convert local square indices and large square indices
                    % to overall square indeices
                    ovi = 3*i - 2 + (ini - 1);
                    ovj = 3*j - 2 + (inj - 1);
                    
                    % remove all possibilities except for the one found
                    % above
                    %p(ovi,ovj,1:k-1) = zeros(1,1,k-1);
                    %p(ovi,ovj,k+1:9) = zeros(1,1,9-k);
                    p(ovi,ovj,:) = zeros(1,1,9);
                    p(ovi,ovj,k) = k;
                end
            end
        end
    end
    
    %% adds 1 iteration and updates u with values just found
    iter = iter + 1;
    
    % updating final u
    for j = 1:9
        for i = 1:9
            v = find(p(i,j,:));
            % if there is only one value possible for an overall square..
            if length(v) == 1
                % ..then take that value and assign it to overall square
                u(i,j) = sum(p(i,j,:));
            end
        end
    end
end

c = check(u);

if isempty(find(u == 0,1)) && c == 0
    return
elseif c == 1
    return
end

%% if it doesn't finish, then tell the user what choices are available in the empties
% if verbose == 1
%     g = find(u == 0);
%     t = zeros(1,9);
%     for n = 1:length(g)
%         row = mod(g(n),9);
%         if row == 0
%             row = 9;
%         end
%         col = ceil(g(n)/9);
%         
%         t(1,:) = p(row,col,:);
%         
%         h = sprintf('%d', find(t));
%         fprintf('The square at [%d,%d] could possibly be: %s\n',row,col,h)
%     end
%     disp(u)
% end

%% sequential trials until a valid matrix is found
g = find(u == 0);
lg = length(g);

% convert numbers 1:81 to indices from 1:9,1:9
row = mod(g,9);
for i = 1:lg
    row(find(row == 0)) = 9;
end
col = ceil(g./9);

ps = zeros(1,lg);
for i = 1:lg
    ps(i) = sum(ceil(p(row(i),col(i),:)/9));
end

% h = sprintf('%d', find(ps));
% fprintf('%s\n',h)

[~,f] = min(ps);

row = row(f);
col = col(f);

b = find(p(row,col,:));
for i = 1:length(b)
    u0 = u;
    u0(row,col) = b(i);
    u0 = sudoku(u0,0);
    if check(u0) == 0
        p(row,col,b(i)) = 0;
    elseif check(u0) == 1
        u = u0;
        return
    end
end

if check(u) == 1
    disp('Could not find a solution. Perhaps this puzzle is unsolvable.')
end
end

% test puzzle
% u = [0 0 0 3 0 0 9 0 0;
%      0 3 0 0 5 9 0 2 7;
%      0 4 0 2 1 8 0 0 6;
%      0 6 0 0 7 1 0 5 0;
%      0 1 4 0 0 0 3 9 0;
%      0 5 0 9 4 0 0 7 0;
%      1 0 0 6 8 2 0 4 0;
%      4 2 0 7 3 0 0 8 0;
%      0 0 8 0 0 4 0 0 0];

% row = mod(g(1),9);
% if row == 0
%     row = 9;
% end
% col = ceil(g(1)/9);
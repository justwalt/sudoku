A = [7 18 27 42 65 49 54 65 78 82 87 98]
[n, bin] = histc(A, unique(A))
multiple = find(n > 1)
index    = find(ismember(bin, multiple))